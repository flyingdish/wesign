package com.massey.fjy.wesign.data.model;

public class Habit {

    public String name;
    public int privilege;
    public int id;
    public String time;

    public Habit() {}

    public Habit(String name) {
        this.name = name;
        this.privilege = Integer.MAX_VALUE;
    }

    public Habit(String name, String time) {
        this.name = name;
        this.privilege = Integer.MAX_VALUE;
        this.time = time;
    }
}
