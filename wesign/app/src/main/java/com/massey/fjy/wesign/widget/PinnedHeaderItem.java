package com.massey.fjy.wesign.widget;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.massey.fjy.wesign.utils.DataUtils;

import java.util.GregorianCalendar;

public class PinnedHeaderItem extends TextView {

    public PinnedHeaderItem(Context context) {
        super(context);
    }

    public PinnedHeaderItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PinnedHeaderItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setText(GregorianCalendar gc) {
        setText(DataUtils.gc2String(gc));
    }
}
