package com.massey.fjy.wesign.view;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.activity.MainActivity;
import com.massey.fjy.wesign.activity.base.WesignActivity;
import com.massey.fjy.wesign.data.SignAdapter;
import com.massey.fjy.wesign.data.model.Habit;
import com.massey.fjy.wesign.fragment.SignFragment;

import java.util.Calendar;

public class AddHabitDialog extends Dialog {

    private View mContentView;
    private EditText mEditText;
    private TextView mAddTextView;
    private TextView mCancelTextView;

    public AddHabitDialog(Context context) {
        super(context);
        initViews();
    }

    private void initViews() {
        mContentView = getLayoutInflater().inflate(R.layout.dialog_add_habit, null, false);
        mEditText = (EditText) mContentView.findViewById(R.id.text);
        mAddTextView = (TextView) mContentView.findViewById(R.id.button);
        mCancelTextView = (TextView) mContentView.findViewById(R.id.cancel);
        mAddTextView.setClickable(true);
        mAddTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AddHabitDialog.this.getOwnerActivity() instanceof WesignActivity) {
                    final String habitName = String.valueOf(mEditText.getText());
                    if (!TextUtils.isEmpty(habitName)) {
                        /*******add clock*******/
                        Calendar currentTime = Calendar.getInstance();
                        TimePickerDialog timePickerDialog = new myTimePickerDialog(getOwnerActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
                                String setTime = hours + ":" + minutes;
                                System.out.println("LOG AddHabitDialog onTimeSet setTime = " + setTime);
                                        ((WesignActivity) AddHabitDialog.this.getOwnerActivity())
                                        .sqlManager().addHabit(new Habit(habitName, setTime));
                                SignFragment signFragment = (SignFragment) ((MainActivity) AddHabitDialog
                                        .this.getOwnerActivity()).getCurrentFragment();
                                ((SignAdapter) signFragment.getAdapter()).updateHabits();
                            }
                        }, currentTime.get(Calendar.HOUR_OF_DAY), currentTime.get(Calendar.MINUTE), true);
                        timePickerDialog.setTitle("Set time");
                        timePickerDialog.show();

                        AddHabitDialog.this.cancel();
                    } else {
                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.input_habit),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        mCancelTextView.setClickable(true);
        mCancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddHabitDialog.this.cancel();
            }
        });
        super.setContentView(mContentView);
    }

    private class myTimePickerDialog extends TimePickerDialog{
        @Override
        protected void onStop() {
            //API 19 will call onTimeSet then cause the bug of same data twice
        }
        public myTimePickerDialog(Context context, OnTimeSetListener callBack,
                                  int hourOfDay, int minute, boolean is24HourView) {
            super(context, callBack, hourOfDay, minute, is24HourView);
        }
    }
}
