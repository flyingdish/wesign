package com.massey.fjy.wesign.data.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

public class SqlManager {

    private SQLiteDatabase db;

    private static SqlManager _instance;

    public SqlManager(Context context) {
        SqlHelper sqlHelper = new SqlHelper(context);
        db = sqlHelper.getWritableDatabase();
    }

    public static SqlManager sqlManager(Context context) {
        if (_instance == null) {
            _instance = new SqlManager(context);
        }
        return _instance;
    }

    public void addHabit(Habit habit) {
        db.beginTransaction();
        try {
            habit.privilege = countHabit();
            db.execSQL("INSERT INTO Habit VALUES(null, ?, ?, ?)", new Object[] {habit.name, habit.privilege, habit.time});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void removeHabit(Habit habit) {
        db.beginTransaction();
        try {
            db.execSQL("DELETE FROM Habit WHERE _id = ?", new Object[] {habit.id});
            db.execSQL("DELETE FROM Sign WHERE habit_id = ?", new Object[] {habit.id});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public int countHabit() {
        int count = 0;
        Cursor c = db.rawQuery("SELECT COUNT(*) FROM Habit", null);
        while (c.moveToNext()) {
            count = c.getInt(0);
        }
        c.close();
        return count;
    }

    public Habit queryHabit(int id) {
        Habit habit = null;
        Cursor c = db.rawQuery("SELECT * FROM Habit WHERE _id = ?", new String[] {"" + id});
        while (c.moveToNext()) {
            habit = new Habit();
            habit.id = c.getInt(c.getColumnIndex("_id"));
            habit.name = c.getString(c.getColumnIndex("name"));
            habit.privilege = c.getInt(c.getColumnIndex("privilege"));
            habit.time = c.getString(c.getColumnIndex("time"));
        }
        c.close();
        return habit;
    }

    public List<Habit> getAllHabits() {
        ArrayList<Habit> habits = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM Habit ORDER BY privilege", null);
        while (c.moveToNext()) {
            Habit habit = new Habit();
            habit.id = c.getInt(c.getColumnIndex("_id"));
            habit.name = c.getString(c.getColumnIndex("name"));
            habit.privilege = c.getInt(c.getColumnIndex("privilege"));
            habit.time = c.getString(c.getColumnIndex("time"));
            habits.add(habit);
        }
        c.close();
        return habits;
    }

    public List<String> getAllHabitsTime() {
        ArrayList<String> habitsTime = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT time FROM Habit ORDER BY privilege", null);
        while (c.moveToNext()) {
            String habitTime = c.getString(c.getColumnIndex("time"));
            habitsTime.add(habitTime);
        }
        c.close();
        return habitsTime;
    }

    public void addSign(Sign sign) {
        db.beginTransaction();
        try {
            db.execSQL("INSERT INTO Sign VALUES(null, ?, ?, ?)", new Object[] {sign.habitId, sign.date, sign.comment});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void removeSign(Sign sign) {
        Cursor c = db.rawQuery("SELECT * FROM Sign WHERE habit_id = ?", new String[] {"" + sign.habitId});
        while (c.moveToNext()) {
            if (c.getString(c.getColumnIndex("sign_date")) != null
                    && sameDate(c.getString(c.getColumnIndex("sign_date")), sign.date)) {
                sign.id = c.getInt(c.getColumnIndex("_id"));
                db.beginTransaction();
                try {
                    db.execSQL("DELETE FROM Sign WHERE _id = ?", new Object[] {sign.id});
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                return;
            }
        }
    }

    public Sign querySign(Habit habit, GregorianCalendar gc) {
        Sign sign = null;
        Cursor c = db.rawQuery("SELECT * FROM Sign WHERE habit_id = ?", new String[] {"" + habit.id});
        while (c.moveToNext()) {
            if (c.getString(c.getColumnIndex("sign_date")) !=  null
                    && sameDate(c.getString(c.getColumnIndex("sign_date")), gc)) {
                sign = new Sign();
                sign.id = c.getInt(c.getColumnIndex("_id"));
                sign.habitId = c.getInt(c.getColumnIndex("habit_id"));
                sign.date = gc;
                sign.comment = c.getString(c.getColumnIndex("comments"));
            }
        }
        c.close();
        return sign;
    }

    private boolean sameDate(String date, GregorianCalendar gc) {
        /**
         * format:
         * java.util.GregorianCalendar[time=1434459154768,areFieldsSet=true,lenient=true,zone=Asia/Shanghai,firstDayOfWeek=1,minimalDaysInFirstWeek=1,ERA=1,YEAR=2015,MONTH=5,WEEK_OF_YEAR=25,WEEK_OF_MONTH=3,DAY_OF_MONTH=16,DAY_OF_YEAR=167,DAY_OF_WEEK=3,DAY_OF_WEEK_IN_MONTH=3,AM_PM=1,HOUR=8,HOUR_OF_DAY=20,MINUTE=52,SECOND=34,MILLISECOND=768,ZONE_OFFSET=28800000,DST_OFFSET=0]
         */
        int pos = date.indexOf(",YEAR=") + 6;
        int year = 0;
        while(date.charAt(pos) >= '0' && date.charAt(pos) <= '9') {
            year *= 10;
            year += date.charAt(pos) - '0';
            pos++;
        }
        pos = date.indexOf(",MONTH=") + 7;
        int month = 0;
        while(date.charAt(pos) >= '0' && date.charAt(pos) <= '9') {
            month *= 10;
            month += date.charAt(pos) - '0';
            pos++;
        }
        pos = date.indexOf(",DAY_OF_MONTH=") + 14;
        int day = 0;
        while(date.charAt(pos) >= '0' && date.charAt(pos) <= '9') {
            day *= 10;
            day += date.charAt(pos) - '0';
            pos++;
        }
        return year == gc.get(Calendar.YEAR) && month == gc.get(Calendar.MONTH) && day == gc.get(Calendar.DATE);
    }

    public int countSign() {
        int count = 0;
        Cursor c = db.rawQuery("SELECT COUNT(*) FROM Sign", null);
        while (c.moveToNext()) {
            count = c.getInt(0);
        }
        c.close();
        return count;
    }

    public List<Sign> getAllSigns() {
        ArrayList<Sign> signs = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM Sign ORDER BY sign_date DESC", null);
        while (c.moveToNext()) {
            Sign sign = new Sign();
            sign.id = c.getInt(c.getColumnIndex("_id"));
            sign.date = getDate(c.getString(c.getColumnIndex("sign_date")));
            sign.comment = c.getString(c.getColumnIndex("comments"));
            sign.habitId = c.getInt(c.getColumnIndex("habit_id"));
            signs.add(sign);
        }
        c.close();
        return signs;
    }

    private GregorianCalendar getDate(String date) {
        /**
         * format:
         * java.util.GregorianCalendar[time=1434459154768,areFieldsSet=true,lenient=true,zone=Asia/Shanghai,firstDayOfWeek=1,minimalDaysInFirstWeek=1,ERA=1,YEAR=2015,MONTH=5,WEEK_OF_YEAR=25,WEEK_OF_MONTH=3,DAY_OF_MONTH=16,DAY_OF_YEAR=167,DAY_OF_WEEK=3,DAY_OF_WEEK_IN_MONTH=3,AM_PM=1,HOUR=8,HOUR_OF_DAY=20,MINUTE=52,SECOND=34,MILLISECOND=768,ZONE_OFFSET=28800000,DST_OFFSET=0]
         */
        int pos = date.indexOf(",YEAR=") + 6;
        int year = 0;
        while(date.charAt(pos) >= '0' && date.charAt(pos) <= '9') {
            year *= 10;
            year += date.charAt(pos) - '0';
            pos++;
        }
        pos = date.indexOf(",MONTH=") + 7;
        int month = 0;
        while(date.charAt(pos) >= '0' && date.charAt(pos) <= '9') {
            month *= 10;
            month += date.charAt(pos) - '0';
            pos++;
        }
        pos = date.indexOf(",DAY_OF_MONTH=") + 14;
        int day = 0;
        while(date.charAt(pos) >= '0' && date.charAt(pos) <= '9') {
            day *= 10;
            day += date.charAt(pos) - '0';
            pos++;
        }
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(year, month, day);
        return gc;
    }
}
