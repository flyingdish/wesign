package com.massey.fjy.wesign.data;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.activity.base.WesignActivity;
import com.massey.fjy.wesign.data.model.Sign;
import com.massey.fjy.wesign.utils.DataUtils;
import com.massey.fjy.wesign.widget.PinnedHeaderItem;
import com.massey.fjy.wesign.widget.PinnedHeaderListView;
import com.massey.fjy.wesign.widget.SignItem;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class MeAdapter extends BaseAdapter implements PinnedHeaderListView.PinnedHeaderAdapter,
        AbsListView.OnScrollListener {

    private List<Object> mData = new ArrayList<>();

    private WesignActivity mActivity;

    public MeAdapter(WesignActivity activity) {
        mActivity = activity;
        mData.addAll(mActivity.sqlManager().getAllSigns());
        if (mData.size() > 0) {
            for (int i = 0; i < mData.size() - 1; ++i) {
                if (!DataUtils.equalDate(((Sign) mData.get(i)).date, ((Sign) mData.get(i + 1)).date)) {
                    mData.add(i + 1, ((Sign) mData.get(i + 1)).date);
                    ++i;
                }
            }
            mData.add(0, ((Sign) mData.get(0)).date);
        }
    }

    @Override
    public int getPinnedHeaderState(int position) {
        if (position < 0 || position >= mData.size()) {
            return PINNED_HEADER_GONE;
        }

        Object item = getItem(position);
        Object itemNext = position < getCount() - 1 ? getItem(position + 1) : null;
        boolean isSection = item instanceof GregorianCalendar;
        boolean isNextSection = (null != itemNext) ? itemNext instanceof GregorianCalendar : false;
        if (!isSection && isNextSection) {
            return PINNED_HEADER_PUSHED_UP;
        }

        return PINNED_HEADER_VISIBLE;
    }

    @Override
    public void configurePinnedHeader(View header, int position, int alpha) {
        if (getItem(position) instanceof GregorianCalendar) {
            ((PinnedHeaderItem) header).setText((GregorianCalendar) getItem(position));
        }
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        Object obj = getItem(position);

        if (obj instanceof GregorianCalendar) {
            if (convertView == null || convertView instanceof SignItem) {
                view = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.pinned_header_item, parent, false);
            } else {
                view = convertView;
            }
            ((PinnedHeaderItem) view).setText((GregorianCalendar) obj);
        } else {
            if (convertView == null || convertView instanceof PinnedHeaderItem) {
                view = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.sign_item, parent, false);
            } else {
                view = convertView;
            }
            ((SignItem) view).setText((Sign) obj, this);
        }
        return view;
    }

    public WesignActivity getActivity() {
        return mActivity;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (view instanceof PinnedHeaderListView) {
            ((PinnedHeaderListView) view).configureHeaderView(firstVisibleItem);
        }
    }
}
