package com.massey.fjy.wesign.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.View;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.activity.base.WesignActivity;
import com.massey.fjy.wesign.fragment.MeFragment;
import com.massey.fjy.wesign.fragment.SignFragment;
import com.massey.fjy.wesign.service.AlarmReceiver;
import com.massey.fjy.wesign.view.AddHabitDialog;
import com.massey.fjy.wesign.widget.BottomTabBar;
import com.massey.fjy.wesign.widget.BottomTabItem;
import com.massey.fjy.wesign.widget.TitleBar;

public class MainActivity extends WesignActivity {

    private SignFragment mFirstFragment;
    private MeFragment mThirdFragment;

    private TitleBar mTitleBar;
    private BottomTabBar mBottomTabBar;

    private int mCurrentFragment = 0;
    private boolean mIsFirstAttached = true;

    private View.OnClickListener editClickListener, finishClickListener;

    private PendingIntent pendingIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        mBottomTabBar = (BottomTabBar) findViewById(R.id.bottom_tab_bar);
        setFirstFragment();

        AddAlarmListeningTask task = new AddAlarmListeningTask();
        task.execute();
    }

    public void setFirstFragment() {
        mCurrentFragment = 0;
        changeBottomImageAndTextColor();

        if (mFirstFragment == null) {
            mFirstFragment = new SignFragment();
        }
        if (mIsFirstAttached) {
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, mFirstFragment).commit();
            mIsFirstAttached = false;
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mFirstFragment).commit();
        }

        mTitleBar.setLeftView(getResources().getString(R.string.add_habit), null);
        mTitleBar.setMiddleView(getResources().getString(R.string.all_habits));
        mTitleBar.setRightView(getResources().getString(R.string.add_habit), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddHabitDialog dialog = new AddHabitDialog(MainActivity.this);
                dialog.setTitle(getResources().getString(R.string.add_your_habit));
                dialog.setOwnerActivity(MainActivity.this);
                dialog.show();
            }
        });
    }

    private void changeBottomImageAndTextColor() {
        for (int i = 0; i < 2; i++) {
            ((BottomTabItem) mBottomTabBar.getChildAt(i)).setTextColor(R.color.light_grey);
        }

        ((BottomTabItem) mBottomTabBar.getChildAt(mCurrentFragment)).setTextColor(R.color.main_blue);

        ((BottomTabItem) mBottomTabBar.getChildAt(0)).setIcon(R.drawable.bottom_icon_signin_grey);
        ((BottomTabItem) mBottomTabBar.getChildAt(1)).setIcon(R.drawable.bottom_icon_me_grey);

        switch (mCurrentFragment) {
            case 0:
                ((BottomTabItem) mBottomTabBar.getChildAt(0)).setIcon(R.drawable.bottom_icon_signin);
                break;
            case 1:
                ((BottomTabItem) mBottomTabBar.getChildAt(1)).setIcon(R.drawable.bottom_icon_me);
                break;
            default:
                break;
        }
    }

    public void setThirdFragment() {
        mCurrentFragment = 1;
        changeBottomImageAndTextColor();

        if (mThirdFragment == null) {
            mThirdFragment = new MeFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mThirdFragment).commit();

        mTitleBar.setLeftView("", null);
        mTitleBar.setMiddleView(getResources().getString(R.string.me));
        mTitleBar.setRightView("", null);
    }


    public Fragment getCurrentFragment() {
        switch (mCurrentFragment) {
            case 0:
                return mFirstFragment;
            case 1:
                return mThirdFragment;
            default:
                return null;
        }
    }

    private class AddAlarmListeningTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... arg0) {
            Intent alarmIntent = new Intent(MainActivity.this, AlarmReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, alarmIntent, 0);
            StartAlarmListening();
            finish();
            return null;
        }
        @Override
        protected void onPreExecute() {
        }
    }

    private void StartAlarmListening() {
        AlarmManager manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        SystemClock.sleep(60000);
        int interval = 60000;
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        System.out.println("LOG MainActivity startAlarmListening");
    }
}
