package com.massey.fjy.wesign.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.activity.base.WesignActivity;
import com.massey.fjy.wesign.data.MeAdapter;
import com.massey.fjy.wesign.widget.PinnedHeaderListView;

public class MeFragment extends Fragment {

    private PinnedHeaderListView view;
    private MeAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = (PinnedHeaderListView) inflater.inflate(R.layout.fragment_me, container, false);
        adapter = new MeAdapter((WesignActivity) getActivity());
        view.setAdapter(adapter);
        view.setOnScrollListener(adapter);
        view.setPinnedHeaderView(LayoutInflater.from(getActivity()).inflate(
                R.layout.pinned_header_item, view, false));
        return view;
    }
}
