package com.massey.fjy.wesign.data.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "com_massey_fjy_wesign.db";
    private static final int DATABASE_VERSION = 1;

    public SqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS Habit" +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, privilege INTEGER, time VARCHAR)");
        db.execSQL("CREATE TABLE IF NOT EXISTS Sign" +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, habit_id INTEGER, sign_date DATE, comments VARCHAR)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
