package com.massey.fjy.wesign.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.massey.fjy.wesign.activity.base.WesignActivity;
import com.massey.fjy.wesign.data.model.Habit;
import com.massey.fjy.wesign.data.model.SqlManager;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.security.auth.callback.CallbackHandler;

public class AlarmReceiver extends BroadcastReceiver {
    private MediaPlayer mMediaPlayer;
    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // an Intent broadcast.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        CharSequence text = String.valueOf(hour) + ":" + String.valueOf(minute);
      //  Toast.makeText(context, text, Toast.LENGTH_SHORT).show();

        Intent serviceIntent = new Intent(context, AlarmService.class);

        // query habits' time from sql
        SqlManager sqlManager = new SqlManager(context);
        List<String> habitsTime = sqlManager.getAllHabitsTime();
        for (int i = 0; i < habitsTime.size(); i++) {
            String habitTime = habitsTime.get(i);
            int index = habitTime.indexOf(':');
            if (index >= 0) {
                int h = Integer.valueOf(habitTime.substring(0, index));
                int m = Integer.valueOf(habitTime.substring(index + 1));
                if (h == hour && m == minute) context.startService(serviceIntent);
            }
        }
    }
}
