package com.massey.fjy.wesign.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.data.SignAdapter;
import com.massey.fjy.wesign.utils.DataUtils;

import java.util.Date;
import java.util.GregorianCalendar;

public class DateSelectItem extends LinearLayout {

    private TextView mLeftArrow;
    private TextView mRightArrow;
    private TextView mDateTv;

    public DateSelectItem(Context context) {
        super(context);
    }

    public DateSelectItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DateSelectItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        mLeftArrow = (TextView) findViewById(R.id.left_arrow);
        mRightArrow = (TextView) findViewById(R.id.right_arrow);
        mDateTv = (TextView) findViewById(R.id.date);
    }

    public void setDate(GregorianCalendar gc, SignAdapter adapter) {
        setMiddleText(gc);
        long currentTimeMillis = System.currentTimeMillis();
        setLeftArrowVisibility(gc, adapter, currentTimeMillis);
        setRightArrowVisibility(gc, adapter, currentTimeMillis);
        setLeftArrowClick(gc, adapter);
        setRightArrowClick(gc, adapter);
    }

    private void setMiddleText(GregorianCalendar gc) {
        mDateTv.setText(DataUtils.gc2StringWithWeek(gc));
    }

    private void setLeftArrowVisibility(GregorianCalendar gc, SignAdapter adapter, long currentTimeMillis) {
        GregorianCalendar today = new GregorianCalendar();
        today.setTime(new Date(currentTimeMillis));
        if (DataUtils.equalDate(gc, today)) {
            mRightArrow.setVisibility(INVISIBLE);
        } else {
            mRightArrow.setVisibility(VISIBLE);
        }
    }

    private void setRightArrowVisibility(GregorianCalendar gc, SignAdapter adapter, long currentTimeMillis) {
        GregorianCalendar weekAgo = new GregorianCalendar();
        weekAgo.setTime(new Date(currentTimeMillis));
        weekAgo.add(GregorianCalendar.DATE, -6);
        if (DataUtils.equalDate(gc, weekAgo)) {
            mLeftArrow.setVisibility(INVISIBLE);
        } else {
            mLeftArrow.setVisibility(VISIBLE);
        }
    }

    private void setLeftArrowClick(final GregorianCalendar gc, final SignAdapter adapter) {
        mLeftArrow.setClickable(true);
        mLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getVisibility() == View.VISIBLE) {
                    gc.add(GregorianCalendar.DATE, -1);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void setRightArrowClick(final GregorianCalendar gc, final SignAdapter adapter) {
        mRightArrow.setClickable(true);
        mRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getVisibility() == View.VISIBLE) {
                    gc.add(GregorianCalendar.DATE, 1);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }
}
