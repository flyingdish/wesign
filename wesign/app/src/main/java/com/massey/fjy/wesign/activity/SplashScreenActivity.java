package com.massey.fjy.wesign.activity;

import android.os.Bundle;
import android.os.Handler;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.activity.base.WesignActivity;

public class SplashScreenActivity extends WesignActivity {

    private boolean done = false;

    private final Runnable timeout = new Runnable() {
        @Override
        public void run() {
            if (!done) {
                startActivity("wesign://main");
                finish();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Handler handler = new Handler();
        // TODO change fast for debug, too fast in use
        handler.postDelayed(timeout, 500);
    }

    public void onDestroy() {
        super.onDestroy();
        done = true;
    }
}
