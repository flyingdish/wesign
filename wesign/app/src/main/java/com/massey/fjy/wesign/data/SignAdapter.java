package com.massey.fjy.wesign.data;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.activity.base.WesignActivity;
import com.massey.fjy.wesign.data.model.Habit;
import com.massey.fjy.wesign.widget.DateSelectItem;
import com.massey.fjy.wesign.widget.HabitItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class SignAdapter extends BaseAdapter {

    private WesignActivity mActivity;
    GregorianCalendar gc = new GregorianCalendar();
    private List<Habit> habits = new ArrayList<>();

    public SignAdapter(WesignActivity activity) {
        mActivity = activity;
        gc.setTime(new Date(System.currentTimeMillis()));
        habits = mActivity.sqlManager().getAllHabits();
    }

    public void updateHabits() {
        habits = mActivity.sqlManager().getAllHabits();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mActivity.sqlManager().countHabit() + 1;
    }

    @Override
    public Object getItem(int position) {
        if (position == 0) {
            return gc;
        }
        return habits.get(position - 1);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        final Object obj = getItem(position);
        if (position == 0) {
            if (convertView != null && convertView instanceof DateSelectItem) {
                view = convertView;
            } else {
                view = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.data_select_item, parent, false);
            }
            ((DateSelectItem) view).setDate((GregorianCalendar) obj, this);
        } else {
            if (convertView != null && convertView instanceof HabitItem) {
                view = convertView;
            } else {
                view = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.habit_item, parent, false);
            }
            ((HabitItem) view).setHabit((Habit) obj, this, gc);
        }
        return view;
    }

    public WesignActivity activity() {
        return mActivity;
    }
}
