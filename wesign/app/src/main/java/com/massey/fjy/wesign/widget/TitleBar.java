package com.massey.fjy.wesign.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.massey.fjy.wesign.R;

public class TitleBar extends LinearLayout {

    private TextView mLeftViewTv;
    private TextView mMiddleView;
    private TextView mRightView;

    private static final int LEFT_TV = -1;
    private static final int LEFT_IV = -2;
    private static final int MIDDLE = -3;
    private static final int RIGHT = -4;

    public TitleBar(Context context) {
        super(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        mLeftViewTv = (TextView) findViewById(R.id.left_view_tv);
        mMiddleView = (TextView) findViewById(R.id.middle_view);
        mRightView = (TextView) findViewById(R.id.right_view);
    }

    public void setLeftView(String text, OnClickListener l) {
        mLeftViewTv.setText(text);
        mLeftViewTv.setVisibility(VISIBLE);
        setViewClick(l, LEFT_TV);
    }

    private void setViewClick(OnClickListener l, int whatView) {
        View view;
        switch (whatView) {
            case LEFT_TV:
                view = mLeftViewTv;
                break;
            case MIDDLE:
                view = mMiddleView;
                break;
            case RIGHT:
                view = mRightView;
                break;
            default:
                return;
        }
        if (l != null) {
            view.setOnClickListener(l);
            view.setClickable(true);
            view.setVisibility(VISIBLE);
        } else {
            view.setClickable(false);
            view.setVisibility(INVISIBLE);
        }
    }

    public void setMiddleView(String text) {
        mMiddleView.setText(text);
    }

    public void setMiddleView(String text, OnClickListener l) {
        mMiddleView.setText(text);
        setViewClick(l, MIDDLE);
    }

    public void setRightView(String text, OnClickListener l) {
        mRightView.setText(text);
        setViewClick(l, RIGHT);
    }
}
