package com.massey.fjy.wesign.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.activity.MainActivity;

public class BottomTabBar extends LinearLayout {

    private BottomTabItem[] items = new BottomTabItem[2];

    public BottomTabBar(Context context) {
        super(context);
    }

    public BottomTabBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BottomTabBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        for (int i = 0; i < 2; i++) {
            items[i] = (BottomTabItem) getChildAt(i);
        }
        setTextForItems();
        setImageForItems();
        setOnClickListenersForItems();
    }

    private void setOnClickListenersForItems() {
        items[0].setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getContext() instanceof MainActivity) {
                    ((MainActivity) getContext()).setFirstFragment();
                }
            }
        });
        items[1].setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getContext() instanceof MainActivity) {
                    ((MainActivity) getContext()).setThirdFragment();
                }
            }
        });
    }

    private void setImageForItems() {
        items[0].setIcon(R.drawable.bottom_icon_signin_grey);
        items[1].setIcon(R.drawable.bottom_icon_me_grey);
    }

    private void setTextForItems() {
        items[0].setText(getResources().getString(R.string.sign));
        items[1].setText(getResources().getString(R.string.me));
    }
}
