package com.massey.fjy.wesign.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.data.MeAdapter;
import com.massey.fjy.wesign.data.model.Sign;

public class SignItem extends LinearLayout {

    private TextView mSignView;

    public SignItem(Context context) {
        super(context);
    }

    public SignItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SignItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        mSignView = (TextView) findViewById(R.id.text);
    }

    public void setText(Sign sign, MeAdapter adapter) {
        mSignView.setText(adapter.getActivity().sqlManager().queryHabit(sign.habitId).name);
    }
}
