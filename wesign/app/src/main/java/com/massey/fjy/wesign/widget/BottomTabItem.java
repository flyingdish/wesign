package com.massey.fjy.wesign.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.massey.fjy.wesign.R;

public class BottomTabItem extends LinearLayout {

    private ImageView mIconView;
    private TextView mTextView;

    public BottomTabItem(Context context) {
        super(context);
    }

    public BottomTabItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BottomTabItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        mIconView = (ImageView) findViewById(R.id.icon);
        mTextView = (TextView) findViewById(R.id.text);
    }

    public void setIcon(int resId) {
        mIconView.setImageResource(resId);
    }

    public void setText(String text) {
        mTextView.setText(text);
    }

    public void setTextColor(int resId) {
        mTextView.setTextColor(getResources().getColor(resId));
    }
}
