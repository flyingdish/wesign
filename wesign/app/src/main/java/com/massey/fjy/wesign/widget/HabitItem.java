package com.massey.fjy.wesign.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.data.SignAdapter;
import com.massey.fjy.wesign.data.model.Habit;
import com.massey.fjy.wesign.data.model.Sign;
import com.massey.fjy.wesign.utils.ViewUtils;
import com.massey.fjy.wesign.view.RemoveHabitDialog;

import java.util.GregorianCalendar;

public class HabitItem extends FrameLayout {

    private TextView mHabitNameTv;
    private TextView mSignTv;
    private SignAdapter adapter;
    private Habit habit;
    private GregorianCalendar gc;

    public HabitItem(Context context) {
        super(context);
    }

    public HabitItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HabitItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        mHabitNameTv = (TextView) findViewById(R.id.habit_name);
        mSignTv = (TextView) findViewById(R.id.sign);
    }

    public void setHabit(Habit habit, SignAdapter adapter, GregorianCalendar gc) {
        mHabitNameTv.setText(habit.name);
        mHabitNameTv.setOnTouchListener(new TouchListener());
        this.habit = habit;
        this.adapter = adapter;
        this.gc = gc;
        if (signed()) {
            mSignTv.setTextColor(getContext().getResources().getColor(R.color.less_main_blue));
            mHabitNameTv.setBackgroundColor(getContext().getResources().getColor(R.color.light_blue));
            mHabitNameTv.setTextColor(getContext().getResources().getColor(R.color.white));
        } else {
            mSignTv.setTextColor(getContext().getResources().getColor(R.color.white));
            mHabitNameTv.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            mHabitNameTv.setTextColor(getContext().getResources().getColor(R.color.black));
        }
    }

    private class TouchListener implements OnTouchListener {

        int startX = 0;
        int initX = 0;

        @Override
        public boolean onTouch(final View v, final MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startX = (int) event.getRawX();
                    initX = startX;
                    break;
                case MotionEvent.ACTION_MOVE:
                    int dx = (int) event.getRawX() - startX;
                    int currentLeft = v.getLeft() + dx;
                    int currentTop = v.getTop();
                    int currentRight = v.getRight() + dx;
                    int currentBottom = v.getBottom();
                    if (currentLeft > ViewUtils.dip2px(getContext(), 100)) {
                        currentLeft = ViewUtils.dip2px(getContext(), 100);
                        currentRight = currentLeft + v.getWidth();
                    } else if (currentLeft < -ViewUtils.dip2px(getContext(), 80)) {
                        currentLeft = -ViewUtils.dip2px(getContext(), 80);
                        currentRight = currentLeft + v.getWidth();
                    }
                    v.layout(currentLeft, currentTop, currentRight, currentBottom);
                    if (currentLeft > ViewUtils.dip2px(getContext(), 60)) {
                        if (signed()) {
                            mSignTv.setTextColor(getContext().getResources().getColor(R.color.white));
                        } else {
                            mSignTv.setTextColor(getContext().getResources().getColor(R.color.less_main_blue));
                        }
                    } else {
                        if (signed()) {
                            mSignTv.setTextColor(getContext().getResources().getColor(R.color.less_main_blue));
                        } else {
                            mSignTv.setTextColor(getContext().getResources().getColor(R.color.white));
                        }
                    }
                    startX = (int) event.getRawX();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    int x = (int) event.getRawX() - initX;
                    if (x > ViewUtils.dip2px(getContext(), 100)) {
                        x = ViewUtils.dip2px(getContext(), 100);
                    } else if (x < -ViewUtils.dip2px(getContext(), 80)) {
                        x = -ViewUtils.dip2px(getContext(), 80);
                    }
                    final int rawX = x;
                    final TranslateAnimation animation = new TranslateAnimation(0,
                            -x, 0, 0);
                    animation.setFillAfter(true);
                    animation.setDuration(200);
                    animation.setInterpolator(getContext(), android.R.anim.accelerate_decelerate_interpolator);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            v.clearAnimation();
                            if (rawX > ViewUtils.dip2px(getContext(), 80)) {
                                if (signed()) {
                                    unSign();
                                } else {
                                    sign();
                                }
                            } else if (rawX < -ViewUtils.dip2px(getContext(), 60)) {
                                RemoveHabitDialog dialog = new RemoveHabitDialog(adapter.activity(), habit);
                                dialog.setTitle(getResources().getString(R.string.confirm_remove_habit));
                                dialog.setOwnerActivity(adapter.activity());
                                dialog.show();
                            }
                            FrameLayout.LayoutParams lp = (LayoutParams) v.getLayoutParams();
                            lp.leftMargin = 0;
                            v.setLayoutParams(lp);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    v.startAnimation(animation);
                    break;
            }
            return false;
        }
    }

    private void sign() {
        Sign sign = new Sign(habit, gc);
        adapter.activity().sqlManager().addSign(sign);
        mHabitNameTv.setBackgroundColor(getContext().getResources().getColor(R.color.light_blue));
        mHabitNameTv.setTextColor(getContext().getResources().getColor(R.color.white));
    }

    private void unSign() {
        Sign sign = new Sign(habit, gc);
        adapter.activity().sqlManager().removeSign(sign);
        mHabitNameTv.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        mHabitNameTv.setTextColor(getContext().getResources().getColor(R.color.black));
    }

    private boolean signed() {
        adapter.activity().sqlManager().countSign();
        return adapter.activity().sqlManager().querySign(habit, gc) != null;
    }
}
