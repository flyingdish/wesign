package com.massey.fjy.wesign.activity.base;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;

import com.massey.fjy.wesign.data.model.SqlManager;

public class WesignActivity extends FragmentActivity {

    public void startActivity(String urlSchema) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlSchema)));
    }

    private static SharedPreferences prefs;

    public SharedPreferences preferences() {
        if (prefs == null) {
            prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        }
        return prefs;
    }

    public SqlManager sqlManager() {
        return SqlManager.sqlManager(this);
    }
}
