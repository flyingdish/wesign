package com.massey.fjy.wesign.view;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.activity.MainActivity;
import com.massey.fjy.wesign.activity.base.WesignActivity;
import com.massey.fjy.wesign.data.SignAdapter;
import com.massey.fjy.wesign.data.model.Habit;
import com.massey.fjy.wesign.fragment.SignFragment;

public class RemoveHabitDialog extends Dialog {
    private View mContentView;
    private EditText mEditText;
    private TextView mRemoveTextView;
    private TextView mCancelTextView;
    private Habit habit;

    public RemoveHabitDialog(Context context, Habit habit) {
        super(context);
        this.habit = habit;
        initViews();
    }

    private void initViews() {
        mContentView = getLayoutInflater().inflate(R.layout.dialog_add_habit, null, false);
        mEditText = (EditText) mContentView.findViewById(R.id.text);
        mEditText.setVisibility(View.GONE);
        mRemoveTextView = (TextView) mContentView.findViewById(R.id.button);
        mRemoveTextView.setText(getContext().getResources().getString(R.string.delete));
        mCancelTextView = (TextView) mContentView.findViewById(R.id.cancel);
        mRemoveTextView.setClickable(true);
        mRemoveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RemoveHabitDialog.this.getOwnerActivity() instanceof WesignActivity) {
                    ((WesignActivity) RemoveHabitDialog.this.getOwnerActivity())
                            .sqlManager().removeHabit(habit);
                    SignFragment signFragment = (SignFragment) ((MainActivity) RemoveHabitDialog
                            .this.getOwnerActivity()).getCurrentFragment();
                    ((SignAdapter) signFragment.getAdapter()).updateHabits();
                    RemoveHabitDialog.this.cancel();
                }
            }
        });
        mCancelTextView.setClickable(true);
        mCancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemoveHabitDialog.this.cancel();
            }
        });
        super.setContentView(mContentView);
    }
}
