package com.massey.fjy.wesign.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.massey.fjy.wesign.R;
import com.massey.fjy.wesign.activity.base.WesignActivity;
import com.massey.fjy.wesign.data.SignAdapter;

public class SignFragment extends Fragment {

    private ListView view;
    private SignAdapter signAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = (ListView) inflater.inflate(R.layout.fragment_sign, container, false);
        signAdapter = new SignAdapter((WesignActivity) getActivity());
        view.setAdapter(signAdapter);
        return view;
    }

    public BaseAdapter getAdapter() {
        return (BaseAdapter) view.getAdapter();
    }
}
