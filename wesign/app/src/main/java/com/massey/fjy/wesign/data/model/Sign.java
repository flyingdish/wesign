package com.massey.fjy.wesign.data.model;

import java.util.GregorianCalendar;

public class Sign {
    public int id;
    public int habitId;
    public GregorianCalendar date;
    public String comment;

    public Sign(Habit habit, GregorianCalendar gc) {
        habitId = habit.id;
        date = gc;
        comment = "";
    }

    public Sign() {

    }
}
