package com.massey.fjy.wesign.utils;

import java.util.GregorianCalendar;

public class DataUtils {

    private static String[] DAYS = new String[] {
            "", "1st", "2nd", "3rd", "4th", "5th",
            "6th", "7th", "8th", "9th", "10th", "11th",
            "12th", "13th", "14th", "15th", "16th", "17th",
            "18th", "19th", "20th", "21st", "22nd", "23rd",
            "24th", "25th", "26th", "27th", "28th", "29th",
            "30th", "31st"
    };

    private static String[] MONTHS = new String[] {
            "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

    private static String[] WEEKS = new String[] {
            "", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };

    public static String gc2StringWithWeek(GregorianCalendar gc) {
        int month = gc.get(GregorianCalendar.MONTH);
        int day = gc.get(GregorianCalendar.DATE);
        int week = gc.get(GregorianCalendar.DAY_OF_WEEK);
        return MONTHS[month] + ". " + DAYS[day] + "    " + WEEKS[week] + ".";
    }

    public static boolean equalDate(GregorianCalendar a, GregorianCalendar b) {
        return a.get(GregorianCalendar.YEAR) == b.get(GregorianCalendar.YEAR)
                && a.get(GregorianCalendar.MONTH) == b.get(GregorianCalendar.MONTH)
                && a.get(GregorianCalendar.DATE) == b.get(GregorianCalendar.DATE);
    }

    public static String gc2String(GregorianCalendar gc) {
        int month = gc.get(GregorianCalendar.MONTH);
        int day = gc.get(GregorianCalendar.DATE);
        int year = gc.get(GregorianCalendar.YEAR);
        return MONTHS[month] + ". " + DAYS[day] + "    " + year;
    }
}
